% vm    - Number of vanishing moments
% j     - scaling function decomposition level i.e. 2^j = M
% log2N - 2^log2N = N, number of samples
% is_per - whether a periodic or boundary wavelet basis should be used
function pisces = get_phi_walsh_pisces(vm, j0, log2N, log2M, is_per);
    
    r = log2N - log2M + 5;
    pisces = get_phi_pisces(vm, j0, r, is_per);
    if (is_per)
        for l = 1:2*vm -1
            pisces{l} = fastwht(pisces{l});
        end 
    else 

        for k = 1:vm
            for l = 1:vm+(k-1)
                pisces{k,l} = fastwht(pisces{k,l});
                pisces{vm+1+k,l} = fastwht(pisces{vm+1+k,l});
            end
        end

        for l = 1:2*vm-1
            pisces{vm+1,l} = fastwht(pisces{vm+1,l});
        end

    end
end


