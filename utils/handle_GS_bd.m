%
% x     - Signal coefficients 
% mode  - 1: forward transform, 2: adjoint operation 
% vm    - Number of vanishing moments
% log2N - 2^log2N = N
% log2M - 2^log2M = M
%
function y = handle_GS_bd(x, mode, vm, j0, log2N, log2M, phi_walsh_pisces)

    is_per = 0;
    N = 2^log2N;
    M = 2^log2M;
    R = log2M;
    q = log2N - log2M;

    if (nargin < 7)
        phi_walsh_pisces = get_phi_walsh_pisces(vm, j0, log2N, log2M, is_per);
    end

    if (or(mode == 1 , strcmpi('notransp', mode)))

        x = IWT_CDJV_noP(x, j0, vm);
        y = kernel_GS_bd(x, mode, vm, log2N, log2M, phi_walsh_pisces);

    else % mode ~= 1 or 'transp' 

        c = kernel_GS_bd(x, mode, vm, log2N, log2M, phi_walsh_pisces);
        y = FWT_CDJV_noP(c, j0, vm);

    end


end





