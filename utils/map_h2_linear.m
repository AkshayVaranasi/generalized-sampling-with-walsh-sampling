% Maps the matlab-linear indices in the N x N matrix to article linear 
% lindices. It is critical that the indices are not sorted. 
function idx_new = map_h2_linear(N,idx);
    if (nargin < 2)
        disp('Error: map_h2_linear requiere two input agrs');
        return;
    end
    N2 = N^2;
    x = zeros(N2,1);
    x(idx) = idx;
    v = h2_linear(x);
    idx_new = zeros(size(idx));    
    
    j = 1;
    for i = 1:N2
        if (v(i) ~= 0)
             idx_new(j) = v(i);
             j = j + 1;
        end
    end
end
