%
% x     - Signal coefficients 
% mode  - 1: forward transform, 2: adjoint operation 
% vm    - Number of vanishing moments
% log2N - 2^log2N = N
% log2M - 2^log2M = M
%
% Note: I might need to change this, so that various values for j0 is accepted
function y = handle_GS2_bd(x, mode, vm, j0, log2N, log2M, phi_walsh_pisces)

    is_per = 0;
    N = 2^log2N;
    M = 2^log2M;
    R = log2M;
    q = log2N - log2M;
    nres = R-j0;
    S = get_wavedec2_s(R, nres);

    if (nargin < 7)    
        phi_walsh_pisces = get_phi_walsh_pisces(vm, j0, log2N, log2M, is_per);
    end

    if (or(mode == 1, strcmpi(mode, 'notransp')))
        % x is M*M vector
        wc = wave_ord_2d_image(x,S,sprintf('db%d', vm));
        sc = IWT2_CDJV_noP(wc,j0,vm);

        Y = zeros(N,N);

        for i = 1:M
            Y(:, i) = kernel_GS_bd(sc(:, i), mode, vm, log2N, R, phi_walsh_pisces);
        end

        for i = 1:N
            Y(i,:) = kernel_GS_bd(Y(i, 1:M)', mode, vm, log2N, R, phi_walsh_pisces);
        end

        y = reshape(Y,N*N,1);
    
    else

        % x is N*N vector
        wc = reshape(x,N,N);
        Y_tmp = zeros(N, M);

        for i = 1:N
            Y_tmp(i, :) = kernel_GS_bd(wc(i,:) , mode, vm, log2N, R, phi_walsh_pisces);
        end

        Y = zeros(M,M);
        for i = 1:M
            Y(:, i) = kernel_GS_bd(Y_tmp(:,i) , mode, vm, log2N, R, phi_walsh_pisces);
        end

        Y = FWT2_CDJV_noP(Y, j0,vm); 

        y = wave_ord_2d_leveled(Y, S)';

    end

end

