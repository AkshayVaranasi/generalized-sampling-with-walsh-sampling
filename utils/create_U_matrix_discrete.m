function U = create_U_matrix_discrete(N, vm, nres, is_per)
    

    wave_name = sprintf('db%d', vm);
    
    R  = round(log2(N));
    j0 = R - nres; 
    S = get_wavedec_s(R, nres);

    if is_per
        handle_IDWT = @(x) waverec(x, S, wave_name);
    else
        handle_IDWT = @(x) IWT_CDJV(x,j0,vm); 
    end

    U = zeros([N,N]);
    for i=1:N
        ei = zeros([N,1]); ei(i) = 1;
        z = handle_IDWT(ei);
        U(:,i) = fastwht(z)*sqrt(N);
    end
end
