classdef Test_handle_GS_per < matlab.unittest.TestCase
    % TestWal tests the wal function
    %
    % USAGE:
    % >> test_case = Test_handle_GS_per;
    % >> res = run(test_case)
    %
    properties
        eps;
    end  
    
    methods (Test)
        % Constructor
        function obj = Test_handle_GS_per(test_case)
            obj.eps = 1e-8;
        end
        
        function test_adjoint_MeqNeq128(test_case)
            eps = test_case.eps;
            
            R = 7;
            q = 0;
            M = 2^R;
            N = 2^(R+q);
            vm = 4;
            j0 = 4;

            I = eye(M);

            G = zeros(N,M);

            mode = 1;  % Forward 
            for i = 1:M
                G(:, i) = handle_GS_per(I(:,i), mode, vm, j0, R+q, R);
                progressbar(i,M);
            end

            mode = 2;  % Adjoint 
            G_adj = zeros(M,N);
            I = eye(N);
            for i = 1:N
                G_adj(:,i) = handle_GS_per(I(:,i), mode, vm, j0, R+q, R);
                progressbar(i,N);
            end
            
            test_case.verifyTrue(norm(G-G_adj','fro') < eps);
        end    
        
    function test_adjoint_M128N512(test_case)
            eps = test_case.eps;
            
            R = 7;
            q = 2;
            M = 2^R;
            N = 2^(R+q);
            vm = 4;
            j0 = 4;

            I = eye(M);

            G = zeros(N,M);

            mode = 1;  % Forward 
            for i = 1:M
                G(:, i) = handle_GS_per(I(:,i), mode, vm, j0, R+q, R);
                progressbar(i,M);
            end

            mode = 2;  % Adjoint 
            G_adj = zeros(M,N);
            I = eye(N);
            for i = 1:N
                G_adj(:,i) = handle_GS_per(I(:,i), mode, vm, j0, R+q, R);
                progressbar(i,N);
            end
            
            test_case.verifyTrue(norm(G-G_adj','fro') < eps);
        end    

    end
    methods (Access=private)
    
    end
end 
