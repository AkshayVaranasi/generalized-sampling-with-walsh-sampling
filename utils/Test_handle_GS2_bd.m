classdef Test_handle_GS2_bd < matlab.unittest.TestCase
    % TestWal tests the wal function
    %
    % USAGE:
    % >> test_case = Test_handle_GS2_bd;
    % >> res = run(test_case)
    %
    properties
        eps;
    end  
    
    methods (Test)
        % Constructor
        function obj = Test_handle_GS2_bd(test_case)
            obj.eps = 1e-8;
        end
        
        function test_adjoint_MeqNeq16(test_case)
            eps = test_case.eps;
            
            R = 5;
            q = 0;
            M = 2^R;
            N = 2^(R+q);
            vm = 3;
            j0 = 4;
            I = eye(M^2);

            G = zeros(N^2,M^2);

            mode = 1;  % Forward 
            for i = 1:M^2
                G(:, i) = handle_GS2_bd(I(:,i), mode, vm, j0, R+q, R);
                progressbar(i,M^2);
            end

            mode = 2;  % Adjoint 
            G_adj = zeros(M^2,N^2);
            I = eye(N^2);
            for i = 1:N^2
                G_adj(:,i) = handle_GS2_bd(I(:,i), mode, vm, j0, R+q, R);
                progressbar(i,N^2);
            end
            
            test_case.verifyTrue( norm(G-G_adj','fro') < eps );
        end    
        
        function test_adjoint_M16N32(test_case)
                eps = test_case.eps;
                
                R = 5;
                q = 1;
                M = 2^R;
                N = 2^(R+q);
                vm = 3;
                j0 = 4;

                I = eye(M^2);

                G = zeros(N^2,M^2);

                mode = 1;  % Forward 
                for i = 1:M^2
                    G(:, i) = handle_GS2_bd(I(:,i), mode, vm, j0, R+q, R);
                    progressbar(i,M^2);
                end

                mode = 2;  % Adjoint 
                G_adj = zeros(M^2,N^2);
                I = eye(N^2);
                for i = 1:N^2
                    G_adj(:,i) = handle_GS2_bd(I(:,i), mode, vm, j0, R+q, R);
                    progressbar(i,N^2);
                end
                
                test_case.verifyTrue(norm(G-G_adj','fro') < eps);
            end    

        end
    methods (Access=private)
    
    end
end 


