%
% x     - Signal coefficients 
% mode  - 1: forward transform, 2: adjoint operation 
% vm    - Number of vanishing moments
% log2N - 2^log2N = N
% log2M - 2^log2M = M
%
function y = handle_GS2_per(x, mode, vm, j0, log2N, log2M, phi_walsh_pisces)
    
    % Store current boundary extension mode
    boundary_extension = dwtmode('status', 'nodisp');
    % Set the correct boundary extension for the wavelets
    dwtmode('per', 'nodisp');

    is_per = 1;
    N = 2^log2N;
    M = 2^log2M;
    R = log2M;
    q = log2N - log2M;
    nres = R-j0;
    wname = sprintf('db%d', vm);

    if (nargin < 7)    
        phi_walsh_pisces = get_phi_walsh_pisces(vm, j0, log2N, log2M, is_per);
    end

    
    if (or(mode == 1, strcmpi(mode, 'notransp')))
        % x is M*M vector
        S = get_wavedec2_s(R, nres);
        sc = waverec2(x, S, wname);

        Y = zeros(N,N);

        for i = 1:M
            Y(:, i) = kernel_GS_per(sc(:, i),  mode, vm, log2N, R, phi_walsh_pisces);
        end

        for i = 1:N
            Y(i,:) = kernel_GS_per(Y(i, 1:M)', mode, vm, log2N, R, phi_walsh_pisces);
        end

        y = reshape(Y,N*N,1);

    else

        % x is N*N vector
        wc = reshape(x,N,N);
        Y_tmp = zeros(N, M);

        for i = 1:N
            Y_tmp(i, :) = kernel_GS_per(wc(i,:)', mode, vm, log2N, R, phi_walsh_pisces);
        end

        Y = zeros(M,M);
        for i = 1:M
            Y(:, i) = kernel_GS_per(Y_tmp(:,i) , mode, vm, log2N, R, phi_walsh_pisces);
        end

        y = wavedec2(Y, nres, wname)';

    end
    
    % Restore dwtmode
    dwtmode(boundary_extension, 'nodisp')

end


