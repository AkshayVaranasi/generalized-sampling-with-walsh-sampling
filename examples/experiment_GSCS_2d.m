function [Q, z] = experiment_GSCS_2d(f, log2N, log2M, vm, is_per, idx, scales, sigma, samples)

    R = log2M;
    M = 2^R;
    N = 2^log2N;
    q = log2N - R;
    spgl1_verbose = 1;

    if(is_per)
        A_handle = @(x, mode) handle_GSCS2_per(x, mode, vm, log2N, R, idx, scales);
    else 
        A_handle = @(x, mode) handle_GSCS2_bd(x, mode, vm, log2N, R, idx, scales);
    end
    y = scales(idx).*samples(idx);

    opts = spgSetParms('verbosity', spgl1_verbose);
    z = spg_bpdn(A_handle, y, sigma, opts); 

    J0 = computeJ0(vm);
    nres = R-J0;
    
    if (is_per)
        S = get_wavedec2_s(R, nres);
        sc = waverec2(z, S, sprintf('db%d', vm));
        T = get_scaling_matrix_per(vm, N, M);
    else 
        J0 = computeJ0(vm);
        nres = R-J0;
        S = get_wavedec2_s(R, nres);
        wc = wave_ord_2d_image(z,S,sprintf('db%d', vm));
        
        sc = IWT2_CDJV_noP(wc, J0, vm);
        T  = get_scaling_matrix_bd(vm, R, log2N);
    end
    
    Q_tmp = zeros(M,N);
    for i = 1:M
        Q_tmp(i, :) = T*sc(i,:)';
    end
    
    Q = zeros(N,N);
    for i = 1:N
        Q(i,:) = T*Q_tmp(:,i);
    end

end

