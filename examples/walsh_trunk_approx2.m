function [s_appr, s_plot,t_plot] = walsh_trunk_approx2(f, N, nbr_samples, samples)
    
    m_samples = round(sqrt(nbr_samples));
    
    
    eps = 1e-14;
    int_factor = 4;
    t_plot = linspace(0, 1-eps, int_factor*N);
    t = linspace( 0, 1-eps, N);
    
    s_appr = 0;
    s_plot = 0;
    
    for n = 1:m_samples
        wn = wal(n-1, t)';
        wn_plot = wal(n-1, t_plot)';
        for m = 1:m_samples
            wm = wal(m-1,t);
            wm_plot = wal(m-1,t_plot);
            W = wn*wm; % The outer product
            W_plot = wn_plot*wm_plot; % The outer product
            s_appr = s_appr + samples(m,n)*W;
            s_plot = s_plot + samples(m,n)*W_plot;
        end
    end

end

