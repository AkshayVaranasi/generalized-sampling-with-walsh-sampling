R = 5;
q = 2;

M = 2^R;
N = 2^(R+q);
is_per = 0;
vm = 2;

dwtmode('per', 'nodisp');

%f = @(x) cos(2*pi*x)  + 0.2 * cos(2*pi*8 *x); 
 f = @(x) (x+1).*(x-0.5).*(x-0.25).*(x+3).*(x-0.6); %+ cos(2*pi*x).*(x <= 0.5); 

samples = walsh_sampling(f,N);

if (is_per)
    G = @(x,mode) handle_GS_per(x, mode, vm, R+q, R); 
else 
    G = @(x,mode) handle_GS_bd(x, mode, vm, R+q, R); 
end

z = lsqr(G, samples,[], 600);

J0 = computeJ0(vm);

nres = R-J0;


if (is_per)
    S = get_wavedec_s(R, nres);
    sc = waverec(z, S, sprintf('db%d', vm));
    A = get_scaling_matrix_per(vm, N, M);
else 
    sc = IWT_CDJV_noP(z, J0, vm);
    A = get_scaling_matrix_bd(vm,R,R+q);
end

x = A*sc; 

figure()
eps = 1e-14;
t = linspace(0,1-eps,N)';
plot(t,f(t));
hold('on');
plot(t,x);
xlabel('t');
title(sprintf('vm: %d, N: %d, M: %d', vm, N, M));

%% Truncated Walsh series
%s = 0;
%for n = 1:N
%    s = s + samples(n)*wal(n-1, t);
%end
%
%hold('on');
%plot(t,s);
%
%%legend({'f(t)', 'GS rec.'});
%%legend({'f(t)', 'GS rec.', 'f_trunk'});
%legend({'f(t)', 'f_trunk'});
%
%fprintf('||f-f_{N,M}||_{inf}: %g\n', norm(f(t)-x, inf));
%fprintf('||f-f_trunk||_{inf}: %g\n', norm(f(t)-s', inf));











