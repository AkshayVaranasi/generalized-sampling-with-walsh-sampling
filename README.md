# Generalized sampling with Walsh functions and wavelet recovery
This software implements the change of basis matrix between a Walsh sampling
basis and orthonormal wavelet recovery basis using fast transforms in one and
two dimensions. It does also support generalized sampling with compressive
sensing. 

## Dependencies
* [SPGL1](http://www.cs.ubc.ca/~mpf/spgl1/) A solver for large-scale sparse reconstruction
* [Fastwht](https://bitbucket.org/vegarant/fastwht/) A fast implementation of
  of matlabs `fwht`-function and an implementation of the Walsh function
* [Wavelab](http://statweb.stanford.edu/~wavelab/) For boundary wavelet
  support.

In addition multiple files from M. Gataric and C. Poon
[software](http://www.damtp.cam.ac.uk/research/afha/code/) is required. As some
of these files need small changes to fit into this framework, all required
files from their work have been included in this repository.  

## Current state of the code
The code should work properly at the moment, but it has undergone large changes
lately, so it could contain some bugs. I also need to document all functions
properly. In future version of this code I plan to remove the dependence on
the Wavelab project, as this project contains some bugs for boundary wavelets. 



